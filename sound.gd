extends HBoxContainer

func _ready():
	$SoundVolumeSlider.value = $SoundVolumeSlider.min_value

func change_volume(volume):
	AudioServer.set_bus_volume_db(0, volume)
	AudioServer.set_bus_mute(0, volume < $SoundVolumeSlider.min_value + 1)
